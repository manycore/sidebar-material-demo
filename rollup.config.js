import path from 'path';
import typescript from 'rollup-plugin-typescript2'
import copy from 'rollup-plugin-copy'

export default {
  // 核心选项
  input: path.join(__dirname, './packages/vm/src/index.ts'),     // 必须
  plugins: [
    typescript({
      tsconfig: path.join(__dirname, './packages/vm/tsconfig.json'),
      extensions: ['.js', '.ts', '.tsx']
    }),
    copy({
      targets: [
        { src: 'manifest.json', dest: 'public' },
      ]
    })
  ],
  output: {  // 必须 (如果要输出多个，可以是一个数组)
    // 核心选项
    file: path.join(__dirname, './public/vm.js'),    // 必须
  },
};
