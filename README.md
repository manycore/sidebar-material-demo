# 侧边栏素材小程序示例

## 介绍

酷家乐工具小程序，侧边栏素材小程序示例，主要演示自定义素材库的功能。

## 项目开发

### 启动本项目

```bash
yarn
yarn dev
```

### 添加小程序入口

打开酷家乐云设计工具，在控制台执行以下代码，可添加小程序入口：

```typescript
__miniappUtils.enableDebug();
__miniappUtils.enableDev();
__miniappUtils.loadApp('sidebar-material-demo', 'http://127.0.0.1:3000/manifest.json');
```

注意：

* 确保向 `__miniappUtils.loadApp()` 传入的 URL 可正常访问。

### 参考资料

* [工具小程序使用文档](https://manual.kujiale.com/idp-sdk/latest/guides/%E5%A6%82%E4%BD%95%E5%BC%80%E5%A7%8B/%E5%90%AF%E5%8A%A8%E5%B0%8F%E7%A8%8B%E5%BA%8F)