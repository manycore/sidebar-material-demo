export const MSG_PREFIX = 'IDP-'

export enum MsgAction {
  GetUserId = 'getUserId',
  Exit = 'exit',
  Fullscreen = "fullscreen",
  StartDragProduct = "startDragProduct"
}