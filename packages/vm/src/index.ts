import { MsgAction, MSG_PREFIX } from "./constant";
import { getAction } from "./utils";

// 挂载展示
IDP.Miniapp.view.defaultFrame.mount(IDP.Miniapp.view.mountPoints.leftPanel);
IDP.Miniapp.view.defaultFrame.onMessageReceive((data: Msg.Data, origin) => {
  const { action, value, id } = data
  const handler: Msg.Handler = msgHandlerMap[getAction(action, Msg.PREFIX)]
  handler(...([] as any[]).concat(value))
    .then(res => {
      return {
        value: res,
        status: true
      }
    })
    .catch(err => {
      return {
        value: err,
        status: false,
      }
    }).then(res => {
      const { value, status } = res
      const msgData: Msg.Data = {
        action,
        value,
        id,
        status,
      }
      IDP.Miniapp.view.defaultFrame.postMessage(msgData)
    })
})

const msgHandlerMap = {
  async [MsgAction.GetUserId]() {
    return IDP.User.getUserId()
  },
  async [MsgAction.Exit]() {
    return IDP.Miniapp.exit()
  },
  async [MsgAction.Fullscreen](isFullscreen = true) {
    // 设置全屏展示
    return IDP.Miniapp.view.setContainerOptions(IDP.Miniapp.view.defaultFrame, {
      windowMode: isFullscreen ? 'fullscreen' : 'windowed',
    });
  },
  async [MsgAction.StartDragProduct](productId: string) {
    return IDP.Interaction.startDragProductAsync(productId)
  },
}

namespace Msg {
  export const PREFIX = MSG_PREFIX
  export type Action = keyof HandlerMap
  export type ActionWithPrefix = `${typeof MSG_PREFIX}${MsgAction}`
  export type Handler = (...params: any[]) => Promise<any>
  export type HandlerMap = typeof msgHandlerMap
  export interface Data {
    action: ActionWithPrefix,
    value: any,
    status: boolean,
    id: string
  }
}

export type { Msg }