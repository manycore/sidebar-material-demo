import { Msg } from "../types"
import { MsgAction, MSG_PREFIX } from "../constant"

export function getAction(actionWithPrefix: Msg.ActionWithPrefix, prefix: typeof MSG_PREFIX) {
  return actionWithPrefix.split(prefix)[1] as MsgAction
}