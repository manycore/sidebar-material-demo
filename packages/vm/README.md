# 工具小程序相关逻辑

> 需要使用到工具小程序的相关逻辑，直接修改src/index.ts中的内容即可

## Usage

``` ts

// src/index.ts中的内容 ...
// 在文件尾部添加后续需要添加的代码

// 小程序核心代码，可以调用酷家乐导出的接口，参照IDP命名空间

// 接收来自默认视图的消息，并调用酷家乐接口此处展示基本toast功能和获取designId
IDP.Miniapp.view.defaultFrame.onMessageReceive(data => {
  if (data === 'get') {
    IDP.Miniapp.view.defaultFrame.postMessage(IDP.Design.getDesignId());
  } else if (data === 'info') {
    IDP.UI.toast.info('info');
  } else if (data === 'warn') {
    IDP.UI.toast.warn('warn');
  } else if (data === 'error') {
    IDP.UI.toast.error('error');
  } else if (data === 'exit') {
    IDP.Miniapp.exit();
  }
});

```
