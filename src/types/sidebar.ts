export enum ESideBarSize {
  S = 244,
  M = 316,
  L = 400,
  XL = 472
}