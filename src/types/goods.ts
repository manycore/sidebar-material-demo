export interface Goods {
  /** 商品分类 */
  type: string
  /** 商品名称 */
  name: string
  /** 模型ID */
  id: string
  /** 推荐标签 */
  tag: string
  /** 风格 */
  style: string
  /** 产品系列 */
  series: string
  /** 品牌 */
  brand: string
  /** 图片 */
  img: string
  /** 尺寸 */
  size: string
  /** 价格 */
  price: string
  /** 库存 */
  stock: string
  /** 购买链接 */
  link: string
}
