import { Goods } from "@/types";
import { Random } from "mockjs";
import goodsJson from "./json/goods.json";

export async function getGoods() {
  const goodsLists: Goods[] = goodsJson.values
  goodsLists.map(goods => {
    goods.stock = getRandStock() + ''
    goods.link = getMockedGoodsLink()
  })
  return goodsLists
}

function getRandStock() {
  return Random.integer(10, 100)
}
function getMockedGoodsLink() {
  return 'https://www.kujiale.com/activities/materialminiapp?kpm=qkWL.c1f945b59e7d0bdf.6774e69.1657874043804'
}