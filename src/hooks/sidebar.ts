import { getSidebarItemColNum, getSidebarSize } from "@/utils"

export function useSidebarSize() {
  const { width } = useWindowSize()
  const size = computed(() => {
    return getSidebarSize(width.value)
  })
  return size
}

export function useSidebarItemColNum() {
  const size = useSidebarSize()
  return computed(() => getSidebarItemColNum(size.value))
}

export function useSidebarColSpan() {
  const itemColNum = useSidebarItemColNum()
  const colSpan = computed(() => {
    return 24 / itemColNum.value
  })
  return colSpan
}