import { ESideBarSize } from "@/types"

export function isWidthBgSize(width: number, size: ESideBarSize) {
  if (width > size) return size
}

export function getSidebarSize(width: number): ESideBarSize {
  const targetSize = [ESideBarSize.XL, ESideBarSize.L, ESideBarSize.M].find((size) => isWidthBgSize(width, size))
  return targetSize ?? ESideBarSize.S
}

export function getSidebarItemColNum(size: ESideBarSize) {
  const ItemColMap = {
    [ESideBarSize.XL]: 4,
    [ESideBarSize.L]: 3,
    [ESideBarSize.M]: 2,
    [ESideBarSize.S]: 2,
  }
  const itemColNum = ItemColMap[size]
  return itemColNum
}